@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Module Details') }}
                        <a class="btn btn-dark" href="{{route('module.index')}}">List</a>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <td>{{$data['record']->name}}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{$data['record']->status}}</td>
                            </tr>

                            <tr>
                                <th>Created At</th>
                                <td>{{$data['record']->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$data['record']->updated_at}}</td>
                            </tr>
                            <tr>
                                <th>Created By</th>
                                <td>{{\App\Models\User::find($data['record']->created_by)->name}}</td>
                            </tr>
                            @if($data['record']->updated_by)
                                <tr>
                                    <th>Updated By</th>
                                    <td>{{\App\Models\User::find($data['record']->updated_by)->name}}</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



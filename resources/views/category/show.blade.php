@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Category Details') }}
                        <a class="btn btn-dark" href="{{route('category.index')}}">List</a>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <td>{{$data['record']->name}}</td>
                            </tr>
                            <tr>
                                <th>Rank</th>
                                <td>{{$data['record']->rank}}</td>
                            </tr>
                            <tr>
                                <th>Slug</th>
                                <td>{{$data['record']->slug}}</td>
                            </tr>
                            <tr>
                                <th>Short Description</th>
                                <td>{{$data['record']->short_description}}</td>
                            </tr>
                            <tr>
                                <th>Meta_Title</th>
                                <td>{{$data['record']->description}}</td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$data['record']->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$data['record']->updated_at}}</td>
                            </tr>
                            <tr>
                                <th>Created By</th>
                                <td>{{\App\Models\User::find($data['record']->created_by)->name}}</td>
                            </tr>
                            @if($data['record']->updated_by)
                                <tr>
                                    <th>Updated By</th>
                                    <td>{{\App\Models\User::find($data['record']->updated_by)->name}}</td>
                                </tr>

                                <tr>
                                    <th>Image</th>
                                    <td><img width="20%" src="{{asset('images/category/' . $data['record']->image)}}" alt=""></td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

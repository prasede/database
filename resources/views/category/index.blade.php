@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Category List') }}
                        <a class="btn btn-dark" href="{{route('category.create')}}">Create</a>
                        <a class="btn btn-dark" href="{{route('category.trash')}}">Trash</a>

                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Rank</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data['records'] as $record)
                                <tr>
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{$record->name}}</td>
                                    <td>{{$record->rank}}</td>
                                    <td>{{$record->created_at}}</td>
                                    <td>
                                        <a href="{{route('category.show',$record->id)}}" class=" btn btn-success">View</a>
                                        <a href="{{route('category.edit',$record->id)}}" class="btn btn-primary">Edit</a>
                                        <form action="{{route('category.destroy',$record->id)}}" method="post">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" name="Delete" value="Delete" class="btn btn-danger">
                                        </form>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


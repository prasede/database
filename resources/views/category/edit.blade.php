@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Category Edit') }}
                        <a class="btn btn-dark" href="{{route('category.index')}}">List</a>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{{route('category.update',$data['record']->id)}}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="title" name="name" class="form-control" value="{{$data['record']->name}}">
                            </div>
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <input type="text" id="slug" name="slug" class="form-control" value="{{$data['record']->slug}}">
                            </div>
                            <div class="form-group">
                                <label for="rank">Rank</label>
                                <input type="number" id="rank" name="rank" class="form-control" value="{{$data['record']->rank}}">
                            </div>
                            <div class="form-group">
                                <label for="short_description">Short Description</label>
                                <textarea name="short_description" class="form-control" id="short_description" cols="30" rows="3">{{$data['record']->short_description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" class="form-control" id="description" cols="30" rows="3">{{$data['record']->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" id="image_file" name="image_file" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="btnSave" value="Update Category" class="btn btn-success">
                                <input type="reset" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

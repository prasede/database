<?php

namespace App\Http\Controllers;

use App\Models\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['records'] = Module::orderby('created_by')->get();
        return view('module.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('module.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //add hidden created_by field
        $request->request->add(['created_by' => Auth::user()->id]);
        //we need model to store data into database
        Module::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = Module::find($id);
        if (!$data['record']) {
            return redirect()->route('module.index');
        }
        return view('module.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        {
            {
                $data['record'] = Module::find($id);
                if (!$data['record']) {
                    return redirect()->route('module.index');
                }
                return view('module.edit', compact('data'));

            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        $data['record'] = Module::find($id);
        if (!$data['record']) {
            return redirect()->route('module.index');
        }
        //add hidden updated_by field
        $request->request->add(['updated_by' => Auth::user()->id]);

        //we need model to update data into database
        $data['record']->update($request->all());
        return redirect()->route('module.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        {
            $data['record'] = Module::find($id);
            if (!$data['record']) {
                return redirect()->route('module.index');
            }
            //delete record
            $data['record']->delete();
            return redirect()->route('module.index');

        }

    }
}

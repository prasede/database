<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        //fetch all data from database using model
        // $data['records'] = Category::all();
        //fetch all data ascending order by rank
        $data['records'] = Category::orderby('rank')->get();
        return view('category.index', compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image_file')) {
            $img = $request->file('image_file');
            $image_name = uniqid() . '_' . $img->getClientOriginalName();
            $img->move('images/category/', $image_name);
            $request->request->add(['image' => $image_name]);
        }
        $request->validate([
            'name' => 'required|unique:categories|max:255',
            'rank' => 'required|integer',
            'slug' => 'required',

        ]);


        //add hidden created_by field
        $request->request->add(['created_by' => Auth::user()->id]);
        //we need model to store data into database
        Category::create($request->all());


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $data['record'] = Category::find($id);
        if (!$data['record']) {
            return redirect()->route('category.index');
        }
        return view('category.show', compact('data'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        {
            $data['record'] = Category::find($id);
            if (!$data['record']) {
                return redirect()->route('category.index');
            }
            return view('category.edit', compact('data'));

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['record'] = Category::find($id);
        if (!$data['record']) {
            return redirect()->route('category.index');
        }
        //add hidden updated_by field
        //$request->request->add(['updated_by' => Auth::user()->id]);

        //we need model to update data into database
        //$data['record']->update($request->all());
        // return redirect()->route('category.index');

        //validation array
        $request->validate([
            'name' => [
                'required',
                'max:255',
                Rule::unique('categories')->ignore($id),
            ],
            'rank' => 'required|integer',
            'slug' => 'required',


        ]);
        $data['record'] = Category::find($id);

        if (!$data['record']) {
            return redirect()->route('category.index');
        }
        //add hidden updated_by field
        $request->request->add(['updated_by' => Auth::user()->id]);

        //update image
        if ($request->hasFile('image_file')) {
            $img = $request->file('image_file');
            $image_name = uniqid() . '_' . $img->getClientOriginalName();
            $img->move('images/category/', $image_name);
            $request->request->add(['image' => $image_name]);
            if ($data['record']->image && file_exists('images/category/' . $data['record']->image)) {
                unlink('images/category/' . $data['record']->image);
            }

        }

        //we need model to update data into database
        $data['record']->update($request->all());
        return redirect()->route('category.index');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['record'] = Category::find($id);
        if (!$data['record']) {
            return redirect()->route('category.index');
        }
        //delete record
        $data['record']->delete();
        return redirect()->route('category.index');

    }

    public function trash()
    {
        $data['records'] = Category::onlyTrashed()->get();
        return view('category.trash', compact('data'));
    }

    public function restore($id)
    {
        $data['records'] = Category::onlyTrashed()->where('id', $id)->restore();
        return redirect()->route('category.index');
    }

    public function forceDelete($id)
    {
        $data = Category::onlyTrashed()->where('id', $id)->get();
        if ($data[0]->image && file_exists('images/category/' . $data[0]->image)) {
            unlink('images/category/' . $data[0]->image);
        }
        $data[0]->forceDelete();
        return redirect()->route('category.index');
    }
}


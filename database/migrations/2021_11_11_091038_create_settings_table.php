<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('address');
            $table->string('email');
            $table->integer('phone');
            $table->string('logo');
            $table->string('mapaddress')->nullable();
            $table->string('fblink')->nullable();
            $table->string('iglink')->nullable();
            $table->string('ytlink')->nullable();
            $table->string('twitterlink')->nullable();
            $table->boolean('status')->default(0);
            $table->softDeletes();
            $table->string('created_by');
            $table->string('updated_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
